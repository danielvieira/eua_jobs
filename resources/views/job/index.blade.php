@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>

<div class="uper">
    <div class="container-fluid mb-3 ">
        @if(session()->get('success'))
            <div class="alert alert-success">
            {{ session()->get('success') }}  
            </div><br />
        @endif
        <a class="btn btn-success mb-3" href="job/create" role="button">Cadastrar Trabalho</a>
        <a class="btn btn-info mb-3" href="worker/create" role="button">Quero me cadastrar</a>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Código</th>
                <th>Título</th>
                <th>Responsavel</th>
                <th>Valor</th>
                <th>Telefone</th>
                <th>Endereço</th>
                <th style="text-align:center">Observações</th>
            </tr>
        </thead>
        <tbody>
            @foreach($jobs as $job)
            <tr>
                <td>{{$job->id}}</td>
                <td>{{$job->title}}</td>
                <td>{{$job->responsible}}</td>
                <td>{{$job->value}}</td>
                <td>{{$job->phone}}</td>
                <td>{{$job->address}}</td>
                <td>{{$job->note}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
<div>
@endsection