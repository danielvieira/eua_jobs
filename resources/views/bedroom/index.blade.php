@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>

<div class="uper">
    <div class="container-fluid mb-3 ">
        @if(session()->get('success'))
            <div class="alert alert-success">
            {{ session()->get('success') }}  
            </div><br />
        @endif
        <a class="btn btn-success mb-3" href="bedroom/create" role="button">Cadastrar Quarto</a>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Código</th>
                <th>Nome</th>
                <th>Valor</th>
                <th>Telefone</th>
                <th>Endereço</th>
                <th style="text-align:center">Observações</th>
            </tr>
        </thead>
        <tbody>
            @foreach($bedrooms as $bedroom)
            <tr>
                <td>{{$bedroom->id}}</td>
                <td>{{$bedroom->name}}</td>
                <td>{{$bedroom->value}}</td>
                <td>{{$bedroom->phone}}</td>
                <td>{{$bedroom->address}}</td>
                <td>{{$bedroom->note}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
<div>
@endsection