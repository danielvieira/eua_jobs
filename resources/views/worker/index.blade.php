@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>

<div class="uper">
    <div class="container-fluid mb-3 ">
        @if(session()->get('success'))
            <div class="alert alert-success">
            {{ session()->get('success') }}  
            </div><br />
        @endif
        <a class="btn btn-info mb-3" href="worker/create" role="button">Quero me cadastrar</a>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Código</th>
                <th>Nome</th>
                <th>Telefone</th>
                <th>E-mail</th>
                <th>Endereço</th>
                <th>Profissão</th>
                <th>Tem<br/>carro</th>
                <th>Fala<br/>Ingles</th>
                <th>Fala<br/>Espanhol</th>
                <th>Carteira<br/>de motorista</th>
                <th style="text-align:center">Observações</th>
            </tr>
        </thead>
        <tbody>
            @foreach($workers as $worker)
            <tr>
                <td>{{$worker->id}}</td>
                <td>{{$worker->name}}</td> 
                <td>{{$worker->phone}}</td> 
                <td>{{$worker->email}}</td> 
                <td>{{$worker->address}}</td> 
                <td>{{$worker->your_job}}</td> 
                <td>{{$worker->have_car}}</td> 
                <td>{{$worker->speak_english}}</td> 
                <td>{{$worker->speak_spanish}}</td>
                <td>{{$worker->have_drive_license}}</td> 
                <td>{{$worker->about}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
<div>
@endsection