@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Meus Dados') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Seu nome') }}</label>
                            
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Seu telefone') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required>

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Seu e-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Qual cidade você mora?') }}</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" required>

                                @if ($errors->has('address'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ingles" class="col-md-4 col-form-label text-md-right">{{ __('Você fala ingles?') }}</label>

                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="speak_english" id="Sim" value="Sim" checked>
                                    <label class="form-check-label" for="temcarro">
                                        Sim
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="speak_english" id="Não" value="Não">
                                    <label class="form-check-label" for="temcarro">
                                        Não
                                    </label>
                                </div>
                                @if ($errors->has('speak_english'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('speak_english') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="speak_spanish" class="col-md-4 col-form-label text-md-right">{{ __('Você fala espanhol?') }}</label>

                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="speak_spanish" id="Sim" value="Sim" checked>
                                    <label class="form-check-label" for="speak_spanish">
                                        Sim
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="speak_spanish" id="Não" value="Não">
                                    <label class="form-check-label" for="speak_spanish">
                                        Não
                                    </label>
                                </div>
                                @if ($errors->has('speak_spanish'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('speak_spanish') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="have_car" class="col-md-4 col-form-label text-md-right">{{ __('Você tem carro?') }}</label>

                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="have_car" id="Sim" value="Sim" checked>
                                    <label class="form-check-label" for="have_car">
                                        Sim
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="have_car" id="Não" value="Não">
                                    <label class="form-check-label" for="have_car">
                                        Não
                                    </label>
                                </div>
                                @if ($errors->has('have_car'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('have_car') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="have_drive_license" class="col-md-4 col-form-label text-md-right">{{ __('Você tem carteira?') }}</label>

                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="have_drive_license" id="have_drive_license" value="have_drive_license" checked>
                                    <label class="form-check-label" for="exampleRadios1">
                                        Sim
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="have_drive_license" id="have_drive_license" value="have_drive_license">
                                    <label class="form-check-label" for="have_drive_license">
                                        Não
                                    </label>
                                </div>
                                @if ($errors->has('have_drive_license'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('have_drive_license') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="your_job" class="col-md-4 col-form-label text-md-right">{{ __('Qual a sua profissão?') }}</label>

                            <div class="col-md-6">
                                <input id="your_job" type="text" class="form-control{{ $errors->has('your_job') ? ' is-invalid' : '' }}" name="your_job" value="{{ old('your_job') }}" required>

                                @if ($errors->has('your_job'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('your_job') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="about" class="col-md-4 col-form-label text-md-right">{{ __('Fale sobre você') }}</label>

                            <div class="col-md-6">
                                <textarea id="about" type="text" style="height:120px" class="form-control{{ $errors->has('about') ? ' is-invalid' : '' }}" name="about" value="{{ old('about') }}" required>
                                </textarea>

                                @if ($errors->has('about'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('about') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <hr class="m-2">
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Crie uma senha') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Repita sua senha') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
