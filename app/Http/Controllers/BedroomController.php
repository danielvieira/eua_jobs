<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BedroomController extends Controller
{
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the nerds
        //$bedrooms = \App\Bedroom::all()->sortBy('id');
        $bedrooms = \App\Bedroom::orderBy('id', 'desc')->get();;

        return view('bedroom.index', compact('bedrooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bedroom/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'note' => 'required',
            'value'=> 'required',
            'address' => 'required'
        ]);
        $bedroom = new \App\Bedroom([
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'note' => $request->get('note'),
            'value'=> $request->get('value'),
            'address' => $request->get('address'),
        ]);
        $bedroom->save();
        return redirect('/bedroom')->with('success', 'Quarto Salvo com Sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
