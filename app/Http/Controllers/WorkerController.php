<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class WorkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workers = \App\Worker::orderBy('id', 'desc')->get();

        return view('worker.index', compact('workers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('worker/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:workers',
            'password' => 'required|string|min:6|confirmed',
            'phone' => 'required|string|max:255', 
            'your_job'=> 'required|string|max:255', 
            'address'=> 'required|string|max:255', 
            'speak_english'=> 'required|string|max:255', 
            'speak_spanish'=> 'required|string|max:255', 
            'have_car'=> 'required|string|max:255', 
            'have_drive_license'=> 'required|string|max:255', 
            'about'=> 'required|string|max:255',
        ]);
        $worker = new \App\Worker([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'phone' => $request->get('phone'), 
            'your_job' => $request->get('phone'), 
            'address'=> $request->get('address'), 
            'speak_english'=> $request->get('speak_english'), 
            'speak_spanish'=> $request->get('speak_spanish'), 
            'have_car'=> $request->get('have_car'), 
            'have_drive_license'=> $request->get('have_drive_license'), 
            'about'=> $request->get('about'),
        ]);
        $worker->save();
        return redirect('/worker')->with('success', 'Cadastro realizado com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
