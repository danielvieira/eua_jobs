<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bedroom extends Model
{
    
    protected $fillable = [
        'name', 
        'phone', 
        'address', 
        'value', 
        'note', 
    ];
}
