<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
    protected $fillable = [
        'name', 
        'email', 
        'password', 
        'phone', 
        'your_job', 
        'address', 
        'speak_english', 
        'speak_spanish', 
        'have_car', 
        'have_drive_license', 
        'about'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
