<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = [
        'title',
        'responsible',
        'phone',
        'address',
        'hourly_rate',
        'note' 
    ];
}
